﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Shifrovanie_XOR
{
    
    public partial class Form1 : Form
    {
       
        public String Filetext; //текст
        public String Shifr_Key; //ключ
        string Shifr_text;
        string DeShifr_text;



        public Form1()
        {
            InitializeComponent();
          
          //  Key_text.Text = Shifr_Key;
            Text_load.Text = Filetext;
            Open_file.Filter = "Text  files(*.txt)|*.txt|All files(*.*)|*.*";
            Save_file.Filter = "Text  files(*.txt)|*.txt|All files(*.*)|*.*";
   
            Shifr_rasshifr.Enabled = false;
            Save_text.Enabled = false;
            Save_shifr.Enabled = false;
        }

        private void Load_text_Click(object sender, EventArgs e)
        {
            if (Open_file.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = Open_file.FileName;
            Filetext = System.IO.File.ReadAllText(filename);
            Text_load.Text = Filetext;
        }

        private void Save_text_Click(object sender, EventArgs e)
        {
            if (Save_file.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = Save_file.FileName;
            System.IO.File.WriteAllText(filename, Text_load.Text);
        }

        private void Key_Click(object sender, EventArgs e)
        {


           
            string key_generate = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            int key_length = 8;
            Shifr_Key = Generate_Random(key_generate, key_length);
            Key_text.Text = Shifr_Key;
        }
        private string Generate_Random(string in_string, int lengt)
        {
            
            Random rnd = new Random();
            StringBuilder gen_random_key = new StringBuilder(lengt);
            int key_pos = 0;
            for (int i = 0; i < lengt; i++)
            {
                key_pos = rnd.Next(0, in_string.Length - 1);
                gen_random_key.Append(in_string[key_pos]);
            }
            return gen_random_key.ToString();
        }
        static string Text_DEL_KEY(string in_string, string key)
        {
            /*while (in_string.Length % key.Length != 0)
            {
                in_string += "\0";
            }
            return in_string;*/

            int dif = ((key.Length - in_string.Length % key.Length) % key.Length);
            string new_data = in_string;
            for (int i = 0; i < dif; i++)
            {
                in_string += "\0";
            }

            return new_data;
        }
        string Shifrovanie_XOR(string in_string, string key)
        {

            Encoding type;
            type = Encoding.Unicode;
            var textInBytes = type.GetBytes(in_string); 
            var keyInBytes = type.GetBytes(key);
            byte[] res = new byte[textInBytes.Length];
            for (var i = 0; i < textInBytes.Length; i++)
            {
                res[i] = Convert.ToByte(textInBytes[i] ^ keyInBytes[i % keyInBytes.Length]);
            }
            return type.GetString(res);
        }

        private void Shifr_rasshifr_Click(object sender, EventArgs e)
        {

            Key_text.Text += "";

            Shifr_Key = Key_text.Text;
            Filetext = Text_DEL_KEY(Text_load.Text, Shifr_Key);
             if (radioButton1.Checked==true)
             {

                 Shifr_text = Shifrovanie_XOR(Filetext, Shifr_Key);
                 Shifr_Text.Text = Shifr_text;

                 Save_shifr.Enabled = true;
             }

             if (radioButton2.Checked)
             {
                 if (Open_file.ShowDialog() == DialogResult.Cancel)
                     return;
                 string filename = Open_file.FileName;
                 string codedtext = System.IO.File.ReadAllText(filename);
                 Shifr_Text.Text = codedtext;

                 DeShifr_text = Shifrovanie_XOR(Shifr_text, Shifr_Key);
                 Text_load.Text = DeShifr_text;
                 Save_text.Enabled = true;
             }
           
        }

       

        private void Save_shifr_Click(object sender, EventArgs e)
        {
           /* if (Save_file.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = Save_file.FileName;
            System.IO.File.WriteAllText(filename, Shifr_Text.Text);*/
             if (Save_file.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = Save_file.FileName;
            System.IO.File.WriteAllText(filename, Shifr_Text.Text);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                Shifr_rasshifr.Enabled = true;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
            {
                Save_shifr.Enabled = false;
            }
        }
    }
}
